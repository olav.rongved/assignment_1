// Store bank balance in object
// Available cash balance
// Outstanding loans

export class Bank {
    constructor(id, balance, currentLoan, hasLoan = false){
    this.id = id;
    this.balance = balance;
    this.currentLoan = currentLoan;
    this.hasLoan = hasLoan 
    }


    // Get functions
    getBalance = function () {
        return this.balance
    }

    getId = function () {
        return this.id
    }

    getCurrentLoan = function (){
        return this.currentLoan
    }

     getLoanStatus = function () {
        return this.hasLoan
    }

    setHasLoan = function (status) {
        this.hasLoan = status
    }

    updateBalance = function (balanceChange) {
        // validate first
        this.balance += balanceChange
    }
    updateLoan = function (loanChange) {
        // validate first
        this.currentLoan += loanChange

        if (this.currentLoan <= 0){
            this.setHasLoan(false)
        }
    }

    requestLoan = function (loanAmount) {
        let isValid = this.validateLoan(loanAmount)
        if (isValid) {
            this.updateBalance(loanAmount)
            this.updateLoan(loanAmount)
            this.setHasLoan(true)
        }
    }

    payLoan = function (downPayment) {
        //Validate amount
        if ((this.getCurrentLoan() - downPayment) <= -1) {
            return "Error! you pay to much!"
        } else {
            this.updateLoan(-1 * downPayment)
        }
        
    }

    transferSalary = function(salary){
        // validate deposit amount
        this.updateBalance(salary)
    }

   

    validateDeposit = function () {

        // Check for current loans
        return "NotYetImplemented"
    }
    validateLoan = function (loanAmount) {
        let isValid = true
        if (isNaN(loanAmount) || loanAmount < 0) {
            alert("Please enter a valid number")

            isValid = false
        }
        if (this.getLoanStatus()) {
            alert("You can only have one loan at the time")
            isValid = false
        }
        if (loanAmount > 2 * this.balance) {
            alert("Your account can only loan double your balance")
            isValid = false
        }
        return isValid

    }

    validatePurchase = function (amount) {

        const currentBalance = this.getBalance()
        if ((currentBalance - amount) < 0) {
            return -1
        } else {
            return 1
        }

    }

    purchaseObject = function (amount){
        this.updateBalance(-1 * amount)
    }



}

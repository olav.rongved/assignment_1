export class Work{

    constructor(id, payBalance, Bank) {
    this.id = id;
    this.payBalance = payBalance;
    this.Bank = Bank
    }

    // Get functions
    getPayBalance = function () {
        return this.payBalance
    }

    getId = function () {
        return this.id
    }

    updatePayBalance = function (amount){
        this.payBalance += amount
        console.log(this.payBalance);
    }

    //TODO: recieve salary might be ambiguous, consider change of name
    receiveSalary = function (salary) {

        // TODO Validate input and stuff maybe?

        let hasLoan = this.Bank.getLoanStatus()
        
        if (hasLoan) {
            // 10 % payment to loan
            let downPayment = 0.1 * salary
            let updatedSalary = salary - downPayment
            
            // TODO: Validate downpayment!!!!
            this.Bank.payLoan(downPayment)
            this.Bank.transferSalary(updatedSalary)


        } else {
            this.Bank.transferSalary(salary)
        }

        this.updatePayBalance(-1 * salary)

    }

    payLoan = function (amount) {
        let hasLoan = this.Bank.getLoanStatus()

        if (hasLoan) {
            this.Bank.payLoan(amount)
        }
    }




    //TODO Get pay - validate with bank for loans

}


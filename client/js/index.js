import { Bank } from './bank.js'
import { Work } from './work.js'
// api stuff
import { fetchComputers } from './api.js'


class App {
    constructor(){

        this.computers = []
    
        this.dummy = new Bank(1, 2000, 0) // TODO move to db
        this.work = new Work(1,500,this.dummy) // TODO move to db
        this.img = document.createElement("img")
        this.selectedComputer = -1
        
        // DOM Elements
        this.elBtnRequestLoan = document.getElementById('btn-request-loan')
        this.elBtnPayLoan = document.getElementById('btn-pay-loan')
        this.elBtnDepositSalary = document.getElementById('btn-deposit-salary')
        this.elComputerSelect = document.getElementById('computers')
        this.elBtnBuyComputer = document.getElementById('btn-buy-computer')
        this.elBtnWork = document.getElementById('btn-work')
        
    }
    
    
    init = async () => {
        
        // Initialize Event listeners
        this.elBtnPayLoan.addEventListener('click', this.handlePayLoanClick.bind(this))
        this.elBtnDepositSalary.addEventListener('click',this.handleDepositClick.bind(this))
        this.elComputerSelect.addEventListener('change',this.handleOnSelectComputer.bind(this))       //TODO: Consider change due to violation for 'click'
        this.elBtnRequestLoan.addEventListener('click', this.handleRequestLoanPrompt.bind(this))
        this.elBtnBuyComputer.addEventListener('click', this.handleBuyComputerPrompt.bind(this))
        this.elBtnWork.addEventListener('click', this.handleWork.bind(this))
        
        try {
            this.computers = await fetchComputers()
            this.computers.forEach(computer => {
                    const elComputer = document.createElement('option')
                    elComputer.innerText = computer.name
                    elComputer.value = computer.id
                    this.elComputerSelect.appendChild(elComputer)
            
            })
        } catch (e) {
            console.log(e)
        }
        this.render()
        
        
    };


    handleWork(){
        this.work.updatePayBalance(100)
        this.render()
    } 

    handleOnSelectComputer = function () {
        this.renderLaptops()

    }


    handlePayLoanClick = function () {

        const currentLoan = this.dummy.getCurrentLoan()
        const currentPayBalance = this.work.getPayBalance()

        const payLoanDifference = currentLoan - currentPayBalance
        if (payLoanDifference > 0) {
            this.work.payLoan(currentPayBalance)
            this.work.updatePayBalance(-1 * currentPayBalance)
        } else {
            this.work.payLoan(currentLoan)
            this.work.updatePayBalance(-1 * currentLoan)
        }
        
        this.render()

    }

    handleDepositClick = function () {
        this.work.receiveSalary(this.work.getPayBalance())
        this.render()

    }


    handleBuyComputerPrompt = function () {

        const options = this.elComputerSelect.options
        const id = options[options.selectedIndex].value - 1
        const confirmation = confirm(`Confirm purchase of ${this.computers[id]['name']}`)

        const price = parseInt(this.computers[id]['price'])
        const currentBalance = this.dummy.getBalance()

        if (this.dummy.validatePurchase(price) === 1 ) {
            this.dummy.purchaseObject(price)
            alert("Purchase confirmed!!")
        } else {
            alert("You dont have enough money!")
        }
        this.render()
        }
    

    handleRequestLoanPrompt = function () {
        const loanAmount = parseInt(prompt("Please enter your required loan:", 0));
        this.dummy.requestLoan(loanAmount)
        this.render()
    }

    render () {
        
        document.getElementById('currentBalance').innerHTML = "Balance: " + (this.dummy.getBalance())
        document.getElementById('currentPayBalance').innerHTML = "Pay Balance: " + (this.work.getPayBalance())
    
        if (this.dummy.hasLoan) {
            document.getElementById('currentLoan').innerHTML = "\n Loan: " + (this.dummy.getCurrentLoan())
        } else {
            document.getElementById('currentLoan').innerHTML = ""
        }
                                                  
    }

    renderLaptops (){

        let img = new Image(100,100);
        const options = this.elComputerSelect.options
        const id = options[options.selectedIndex].value - 1


        img = document.getElementById('computer-img').innerHTML = `
                                     <img src="${this.computers[id]['image']}" alt="HTML5 Icon" width="128" height="128">
                                      `

        
        document.getElementById('computerName').innerHTML = `<div style="font-size:1.5vw" align="left">
                                                            NAME: ${this.computers[id]['name']},<br>
                                                            DESCRIPTION: ${this.computers[id]['description']},<br>
                                                            SPECS: ${this.computers[id]['specs']},<br>
                                                            RATING: ${this.computers[id]['rating']},<br>
                                                            PRICE: ${this.computers[id]['price']}<br>
                                                            </div>
                                                            `



    }

    }


new App().init()